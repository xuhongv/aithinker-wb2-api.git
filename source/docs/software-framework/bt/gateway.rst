蓝牙网关
======

.. raw:: html

   <style>
   body {counter-reset: h2}
     h2 {counter-reset: h3}
     h2:before {counter-increment: h2; content: counter(h2) ". "}
     h3:before {counter-increment: h3; content: counter(h2) "." counter(h3) ". "}
     h2.nocount:before, h3.nocount:before, { content: ""; counter-increment: none }
   </style>

--------------

这个32-G网关能否进行二次开发
--------------

不支持

蓝牙网关和蓝牙模组之间是通过什么方式通信的，ble连接还是ble mesh交互
-----------------
BLE MESH

这款32-G产品，里面是esp32的芯片吗 
----------
ESP32D0WDQ5


蓝牙网关能连接多少个蓝牙终端呢 
----------
400个

这个32-G网关里面就一个esp32吗
----------
ESP32+网口

这个32-G里面是由什么组成
----------
ESP32模块、网口 、拨码开关、 按键等

32-G以太网用不了么
----------
可以使用的

32-G蓝牙网关和蓝牙模块可以多远距离通讯
----------
100米

32-G支持 蓝牙mesh吗
----------
支持的

32-G可以连接多少个蓝牙设备
----------
500个

32-G蓝牙网关支持Arduino吗
----------
不支持

32-G可以接涂鸦sig mesh蓝牙模块吗？
----------
不行

32-G可以蓝牙跟蓝牙设备通讯，并通过wifi把数据透传上网络上去吗？
----------
WIFI就是直接上网

32-G可以配套贵司的哪些蓝牙模块吗？
----------
TB和PB系列都可以