API指南
=============

| 这里汇集了Ai-WB2系列使用开发所涉及到的各类API

WiFi系列
---------
.. toctree::
   :maxdepth: 1
   :glob:

   WiFi/*

蓝牙系列
---------

.. toctree::
   :maxdepth: 1
   :glob:

   bt/*


外设系列
---------

.. toctree::
   :maxdepth: 1
   :glob:

   peripherals/*


存储系列
---------

.. toctree::
   :maxdepth: 1
   :glob:

   storage/*


系统系列
---------

.. toctree::
   :maxdepth: 1
   :glob:

   system/*


协议系列
---------

.. toctree::
   :maxdepth: 1
   :glob:

   protocol/*

