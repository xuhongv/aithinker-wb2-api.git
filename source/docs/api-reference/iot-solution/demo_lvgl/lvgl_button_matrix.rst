安信可LVGL button_matrix示例
======

.. raw:: html

   <style>
   body {counter-reset: h2}
     h2 {counter-reset: h3}
     h2:before {counter-increment: h2; content: counter(h2) ". "}
     h3:before {counter-increment: h3; content: counter(h2) "." counter(h3) ". "}
     h2.nocount:before, h3.nocount:before, { content: ""; counter-increment: none }
   </style>

--------------


=========
LVGL button_matrix Example
=========

The screen used in this routine is a 240 * 240 TFT display screen driven by ST7789。


LVGL config
:::::::::

LVGL configuration is unified in the `lv_conf.h` file. You can configure the model, size and drive pin of the display,
 
Model configuration
:::::::::

 .. code-block:: c
  
   #define LV_DISPLAY_ST7789

SPI pin configuration
:::::::::

  .. code-block:: c
    
   #define ST7789_DC 4
   #define ST7789_CS 5
   #define ST7789_RST 14
   #define ST7789_CLK 3
   #define ST7789_MOSI 12
   #define ST7789_MISO 17

Display direction configuration
:::::::::

  .. code-block:: c
  
    #define CONFIG_LV_DISPLAY_ORIENTATION 1

- CONFIG_ LV_ DISPLAY_ ORIENTATION can be configured as 0 to 3, respectively identifying four display directions



:::::::::
Display screen size configuration


.. code-block:: c

 #define LV_HOR_RES_MAX 240
 #define LV_VER_RES_MAX 240
 #define MY_DISP_VER_RES LV_VER_RES_MAX
 #define MY_DISP_HOR_RES LV_HOR_RES_MAX

:::::::
RUN 

example 1

.. image:: lvgl_button_matrix_img/example1.jpg

example 2

.. image:: lvgl_button_matrix_img/example2.jpg

example 3

.. image:: lvgl_button_matrix_img/example3.jpg

Troubleshooting
:::::::::

For any technical queries, please open an [issue](https://github.com/Ai-Thinker-Open/Ai-Thinker-WB2/issues) on GitHub. We will get back to you soon.


