安信可 LVGL示例
======

.. raw:: html

   <style>
   body {counter-reset: h2}
     h2 {counter-reset: h3}
     h2:before {counter-increment: h2; content: counter(h2) ". "}
     h3:before {counter-increment: h3; content: counter(h2) "." counter(h3) ". "}
     h2.nocount:before, h3.nocount:before, { content: ""; counter-increment: none }
   </style>

--------------


=========
LVGL Example
=========

The display driver used in this routine is SSD1306,and the size is `128 * 64`.I2C communication mode。


LVGL config
:::::::::

LVGL configuration is unified in the `lv_conf.h` file. You can configure the model, size and drive pin of the display,
 
Model configuration
:::::::::

 .. code-block:: c
  
   #define LV_DISPLAY_SSD1306

I2C pin configuration
:::::::::

  .. code-block:: c
    
    #define OLED_IIC_SCL 12
    #define OLED_IIC_SDA 3

Display direction configuration
:::::::::

  .. code-block:: c
  
    #define LV_DISPLAY_ORIENTATION_LANDSCAPE 
   // #define LV_DISPLAY_ORIENTATION_LANDSCAPE_INVERTED 

- LV_DISPLAY_ORIENTATION_LANDSCAPE:Normal display
- LV_DISPLAY_ORIENTATION_LANDSCAPE_INVERTED: Mirror Display


:::::::::
Display screen size configuration


.. code-block:: c

 #define MY_DISP_HOR_RES    128
 #define MY_DISP_VER_RES    64
 #define LV_VER_RES_MAX     10

:::::::
RUN 

.. image:: img/ssd1306.jpg

Troubleshooting
:::::::::

For any technical queries, please open an [issue](https://github.com/Ai-Thinker-Open/Ai-Thinker-WB2/issues) on GitHub. We will get back to you soon.


