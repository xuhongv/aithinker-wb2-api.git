安信可 NTC示例
======

.. raw:: html

   <style>
   body {counter-reset: h2}
     h2 {counter-reset: h3}
     h2:before {counter-increment: h2; content: counter(h2) ". "}
     h3:before {counter-increment: h3; content: counter(h2) "." counter(h3) ". "}
     h2.nocount:before, h3.nocount:before, { content: ""; counter-increment: none }
   </style>

--------------


=========
Ai-WB2 Series SoC Module Temperature Measurement via NTC Example
=========

Hardware Setup and Wiring
:::::::::

.. image:: img/schematic.jpg
.. image:: img/wiring.jpg


Solve NTC Resistor Value
:::::::::

.. image:: img/schematic.jpg

make flash


Run
:::::::::
.. image:: img/1.jpg
.. image:: img/2.jpg
.. image:: img/3.jpg

Relationship between Resistor Value & Temperature
:::::::::
https://github.com/Ai-Thinker-Open/Ai-Thinker-WB2/blob/main/applications/iot-solution/demo_ntc/img/RT.csv


Troubleshooting
:::::::::

For any technical queries, please open an [issue](https://github.com/Ai-Thinker-Open/Ai-Thinker-WB2/issues) on GitHub. We will get back to you soon.
