安信可 radar_node示例
======

.. raw:: html

   <style>
   body {counter-reset: h2}
     h2 {counter-reset: h3}
     h2:before {counter-increment: h2; content: counter(h2) ". "}
     h3:before {counter-increment: h3; content: counter(h2) "." counter(h3) ". "}
     h2.nocount:before, h3.nocount:before, { content: ""; counter-increment: none }
   </style>

--------------


=========
Ai-WB2 Series SoC Module Reads Rd-01 Sensor via UART example
=========

Hardware Setup and Wiring
:::::::::

+----------------------------------+-------------------+
| Ai-WB2 Series SoC Module Pinout  |Rd-01  Pinout      |
+==================================+===================+
|IO4                               | RXD               | 
+----------------------------------+-------------------+
|IO3                               | TXD               | 
+----------------------------------+-------------------+
|3V3                               | VCC               | 
+----------------------------------+-------------------+
|GND                               | GND               | 
+----------------------------------+-------------------+

Build and Flash
:::::::::

make -j

make flash


NOTE
:::::::::

+----------------------------------+-------------------+
| Rd-01 status                     |Value              |
+==================================+===================+
| NOT TARGET                       |0x00               | 
+----------------------------------+-------------------+
|ACTIVE TARGET                     | 0x01              | 
+----------------------------------+-------------------+
|STATIC TARGET                     | 0x02              | 
+----------------------------------+-------------------+
| ACTIVE & STATIC TARGET           | 0x03              | 
+----------------------------------+-------------------+

- LOCAL UDP LISTEN PORT `7878`


Troubleshooting
:::::::::

For any technical queries, please open an [issue](https://github.com/Ai-Thinker-Open/Ai-Thinker-WB2/issues) on GitHub. We will get back to you soon.
