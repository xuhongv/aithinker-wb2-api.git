安信可 IR示例
======

.. raw:: html

   <style>
   body {counter-reset: h2}
     h2 {counter-reset: h3}
     h2:before {counter-increment: h2; content: counter(h2) ". "}
     h3:before {counter-increment: h3; content: counter(h2) "." counter(h3) ". "}
     h2.nocount:before, h3.nocount:before, { content: ""; counter-increment: none }
   </style>





Ai-WB2 Series SoC Module IR Receive & Transmit Example
--------------

+----------------------------------+-------------------+
| Ai-WB2 Series SoC Module Pinout  | Model Name        |
+==================================+===================+
|IO12                              | IR Receiver Output| 
+----------------------------------+-------------------+
|IO11                              |Cathode of IR LED  |
+----------------------------------+-------------------+
|VCC                               |Anode of IR LED    |
+----------------------------------+-------------------+
|VCC                               |IR Receiver Vcc    |
+----------------------------------+-------------------+
|GND                               |IR Receiver GND    |
+----------------------------------+-------------------+

.. image:: img/wiring.jpg

Build and Flash
:::::::::

make -j

make flash

Run
:::::::::

.. image:: img/demo.jpg
 

Logic Analyzer Output
:::::::::

.. image:: img/logic_analyzer.jpg


Troubleshooting
:::::::::

For any technical queries, please open an [issue](https://github.com/Ai-Thinker-Open/Ai-Thinker-WB2/issues) on GitHub. We will get back to you soon.


