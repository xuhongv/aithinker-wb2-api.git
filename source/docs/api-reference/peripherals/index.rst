安信可 peripherals参考
=============


UART示例
---------

.. toctree::
   :maxdepth: 1
   :glob:

   uart/*


ADC示例
---------

.. toctree::
   :maxdepth: 1
   :glob:

   demo_adc/*


GPIO示例
---------

.. toctree::
   :maxdepth: 1
   :glob:

   demo_gpio/*


I2C示例
---------

.. toctree::
   :maxdepth: 1
   :glob:

   demo_i2c/*


PWM示例
---------

.. toctree::
   :maxdepth: 1
   :glob:

   demo_pwm/*


SPI示例
---------

.. toctree::
   :maxdepth: 1
   :glob:

   demo_spi/*


IR示例
---------

.. toctree::
   :maxdepth: 1
   :glob:

   demo_ir/*


Timer示例
---------

.. toctree::
   :maxdepth: 1
   :glob:

   demo_timer/*
