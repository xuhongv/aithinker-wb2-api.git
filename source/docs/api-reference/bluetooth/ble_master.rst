安信可 ble_master示例
======

.. raw:: html

   <style>
   body {counter-reset: h2}
     h2 {counter-reset: h3}
     h2:before {counter-increment: h2; content: counter(h2) ". "}
     h3:before {counter-increment: h3; content: counter(h2) "." counter(h3) ". "}
     h2.nocount:before, h3.nocount:before, { content: ""; counter-increment: none }
   </style>

--------------


=========
概述: 
=========

实现蓝牙主机扫描透步骤如下：
1、相关硬件和基础服务初始化；
2、设置扫描参数；
3、设置连接参数；
4、设置配对参数（可选）；
5、启动协议栈，开始运行；
6、等待相关事件，及事件处理，例如扫描事件等；




开发环境搭建
:::::::::
在Ubuntu环境下，打开终端，安装make，命令：sudo apt-get update、sudo apt-get install make。

获取sdk代码：git clone https://github.com/Ai-Thinker-Open/Ai-Thinker-WB2

在Ai-Thinker-WB2/applications/ble下创建ble_master工程,仿照applications/get-started/hello工程写Makefile、proj_config.mk等编译规则文件以及自己的代码到ble_master工程里，使用make命令进行编译。


烧录及测试
:::::::::
准备好一块NodeMCU-Serices Ai-WB2-12F-Kit开发板连接电脑，打开Bouffalo Lab Dev Cube 1.8.0 for AiThinker -BL602/604烧录软件导入要下载的固件，配置好下载COM口和波特率等参数，然后点击软件上的Open UART按钮连接设备,再长按开发板上的BURM按键，同时短按一下EN按键，进入下载模式，最后点击烧录软件上Create&Download开始烧录。烧录软件上的配置截图如下：

.. image:: img/ble_master.png

打开串口调试助手，可以看到打印的扫描BLE设备信息，连接从机成功的信息

.. image:: img/ble_master_uart.png

Troubleshooting
:::::::::

For any technical queries, please open an [issue](https://github.com/Ai-Thinker-Open/Ai-Thinker-WB2/issues) on GitHub. We will get back to you soon.
