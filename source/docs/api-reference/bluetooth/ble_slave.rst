安信可 ble_slave示例
======

.. raw:: html

   <style>
   body {counter-reset: h2}
     h2 {counter-reset: h3}
     h2:before {counter-increment: h2; content: counter(h2) ". "}
     h3:before {counter-increment: h3; content: counter(h2) "." counter(h3) ". "}
     h2.nocount:before, h3.nocount:before, { content: ""; counter-increment: none }
   </style>

--------------


=========
Example: Ai-WB2 Series SoC Module ble slave usage
=========

Hardware Setup and Wiring
:::::::::

+----------------------------------+-------------------+
| Ai-WB2 Series SoC Module Pinout  |Peripheral Pinout  |
+==================================+===================+
|3V3                               |  VCC              | 
+----------------------------------+-------------------+
|GND                               |  GND              | 
+----------------------------------+-------------------+

Build and Flash
:::::::::
make -j

make flash


Test
:::::::::
More detail to look for the foler docs.


Troubleshooting
:::::::::

For any technical queries, please open an [issue](https://github.com/Ai-Thinker-Open/Ai-Thinker-WB2/issues) on GitHub. We will get back to you soon.
