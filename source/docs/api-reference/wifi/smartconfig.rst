安信可 smartconfig 示例
======

.. raw:: html

   <style>
   body {counter-reset: h2}
     h2 {counter-reset: h3}
     h2:before {counter-increment: h2; content: counter(h2) ". "}
     h3:before {counter-increment: h3; content: counter(h2) "." counter(h3) ". "}
     h2.nocount:before, h3.nocount:before, { content: ""; counter-increment: none }
   </style>

--------------


=========
smartconfig example
=========


if you want usb smartconfig,plese call 

::

    wifi_smartconfig_v1_start();

if you want usb airkiss ,plese call 

::
   
    wifi_airkiss_v1_start();

App and mini program souce code:
https://docs.ai-thinker.com/esp8266/smartconfig

Troubleshooting
:::::::::

For any technical queries, please open an [issue](https://github.com/Ai-Thinker-Open/Ai-Thinker-WB2/issues) on GitHub. We will get back to you soon.
