安信可 Iperf Example示例
======

.. raw:: html

   <style>
   body {counter-reset: h2}
     h2 {counter-reset: h3}
     h2:before {counter-increment: h2; content: counter(h2) ". "}
     h3:before {counter-increment: h3; content: counter(h2) "." counter(h3) ". "}
     h2.nocount:before, h3.nocount:before, { content: ""; counter-increment: none }
   </style>

--------------


=========
Introduction
=========

This example shows how to test Ai-WB2 serial module's network throughput using Iperf.

How to use example
:::::::::

Open the project,set WIFI_SSID,WIFI_PASSWORD at line 3,line 4 in wifi_execute.c file.
For example : 

.. code-block:: c

  #define STA_SSID "Ai-WB2-x"
  #define STA_PASSWORD "12345678"



Finally,running command below to compile project on ubuntu.

make -j8


Burn firmware
:::::::::

Running command below to burn firmware after connecting WB2 serial development board to ubuntu.

make flash p=/dev/ttyUSB0 b=921600

Debug
:::::::::

Preparation of iperf tool
----------------

Get the installation package of the iperf tool (version: 2.0.9) from `iperf download link  <https://iperf.fr/iperf-download.php#windows>`__

Get the "iperf-2.0.9-win32" folder by unzipping the " iperf-2.0.9-win32.zip " compressed package. Go to this folder directory and run " cmd " command to open the command line window.

Preparation of serial tool
----------------------

Get the serial tool from `Serial tool download link  <https://docs.ai-thinker.com/_media/tools/aithinker_serial_tool_v1.2.3.7z>`__

Unzip the serial tool compressed package. Open the serial port debugging assistant software,select the COM port of the development board,and press the reset button on development board to run the program.

Debug
:::::::::
running "wifi_sta_connect SSID PASSWORD" command on module through serial tool to connect to WiFi, as below:（PC need to connect to the same WiFi）

.. image:: img/WB2_WIFI.png

1）setting module as UDP client and PC as server

running "iperf.exe -s -u -i 1" command on PC to setup UDP server, as below: 

.. image:: img/PC_UDP_SERVER.png

running "ipu 192.168.43.8" command on module through serial tool to connect to UDP server, as below: (“192.168.43.8” is server IP)

.. image:: img/WB2_UDP_CLIENT.png


2）setting module as TCP client and PC as server

running "iperf.exe -s -i 1" command on PC to setup TCP server, as below: 

.. image:: img/PC_TCP_SERVER.png

running "ipc 192.168.43.8" command on module through serial tool to connect to TCP server, as below:  (“192.168.43.8” is server IP)

.. image:: img/WB2_TCP_CLIENT.png

3）setting module as UDP server and PC as client

 running "ipus" command on module through serial tool to setup UDP server, as below: 

.. image:: img/WB2_UDP_SERVER.png

running "iperf.exe -u -c 192.168.43.27 -t 360 -i 1" command on PC to connect to UDP server, as below:  (“192.168.43.27” is server IP)

.. image::  img/PC_UDP_CLIENT.png


4）setting module as TCP server and PC as client

 running "ips" command on module through serial tool to setup TCP server, as below: 

.. image:: img/WB2_TCP_SERVER.png

running "iperf.exe -c 192.168.43.27 -t 360 -i 1" command on PC to connect to TCP server, as below:  (“192.168.43.27” is server IP)

.. image:: img/PC_TCP_CLIENT.png


Troubleshooting
:::::::::

For any technical queries, please open an [issue](https://github.com/Ai-Thinker-Open/Ai-Thinker-WB2/issues) on GitHub. We will get back to you soon.


=========
简介
=========

本例程主要介绍如何通过iperf测试Ai-WB2系列模组的吞吐量。

如何使用该例程
:::::::::

打开项目，在 “wifi_execute.c” 文件第3行以及第4行设置指定wifi的账号和密码，STA_SSID、STA_PASSWORD，如下：

.. code-block:: c

  #define STA_SSID "Ai-WB2-x"
  #define STA_PASSWORD "12345678"



最后，在ubuntu下运行以下指令编译工程。

make -j8


烧录固件
:::::::::

将WB2开发板连接ubuntu之后，运行以下指令烧录固件。

make flash p=/dev/ttyUSB0 b=921600

调试
:::::::::

iperf工具的准备
----------------

从iperf下载链接 `iperf download link  <https://iperf.fr/iperf-download.php#windows>`__ 中获取iperf（2.0.9版本）工具；

解压“iperf-2.0.9-win32.zip”压缩包，获取“iperf-2.0.9-win32”文件夹，进入该文件夹下运行cmd指令，打开指令运行窗口；

串口工具的准备
----------------------

从串口工具下载链接 `Serial tool download link  <https://docs.ai-thinker.com/_media/tools/aithinker_serial_tool_v1.2.3.7z>`__ 中获取串口工具；

解压串口工具包，打开“串口调试助手软件”，选择并打开开发板的COM口，按下复位键启动程序；

联调
:::::::::
通过运行指令“wifi_sta_connect SSID PASSWORD”连接指定wifi。（调试用PC机需连接同一个WIFI）

.. image:: img/WB2_WIFI.png

1）在指令运行终端运行“iperf.exe -s -u -i 1”指令创建UDP服务器，如下：

.. image:: img/PC_UDP_SERVER.png

在串口助手中，运行“ipu 192.168.43.8”指令连接UDP服务器，如下：（其中“192.168.43.8”为服务器的IP地址）

.. image:: img/WB2_UDP_CLIENT.png


2）在指令运行终端中运行“iperf.exe -s -i 1”指令创建TCP服务器，如下：

.. image:: img/PC_TCP_SERVER.png

在串口助手中，运行“ipc 192.168.43.8”指令连接TCP服务器，如下：（其中“192.168.43.8”为服务器的IP地址）

.. image:: img/WB2_TCP_CLIENT.png

3）在串口助手中，运行“ipus”指令创建UDP服务器，如下：

.. image:: img/WB2_UDP_SERVER.png

 在指令运行终端运行“iperf.exe -u -c 192.168.43.27 -t 360 -i 1”指令连接UDP服务器，如下：（其中“192.168.43.27”为服务器的IP地址）

.. image::  img/PC_UDP_CLIENT.png


4）在串口助手中，运行“ips”指令创建TCP服务器，如下：

.. image:: img/WB2_TCP_SERVER.png

在指令运行终端运行“iperf.exe -c 192.168.43.27 -t 360 -i 1”指令连接TCP服务器，如下：（其中“192.168.43.27”为服务器的IP地址）

.. image:: img/PC_TCP_CLIENT.png

问题排除
:::::::::

若有任何问题，请在github中提交一个[issue](https://github.com/Ai-Thinker-Open/Ai-Thinker-WB2/issues)，我们会尽快回复。