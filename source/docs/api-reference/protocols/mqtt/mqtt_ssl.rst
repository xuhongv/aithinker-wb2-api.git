安信可 MQTT_SSL示例
======

.. raw:: html

   <style>
   body {counter-reset: h2}
     h2 {counter-reset: h3}
     h2:before {counter-increment: h2; content: counter(h2) ". "}
     h3:before {counter-increment: h3; content: counter(h2) "." counter(h3) ". "}
     h2.nocount:before, h3.nocount:before, { content: ""; counter-increment: none }
   </style>

--------------


=========
MQTT_SSL Example
=========

Configure WiFi Connected
>>>>>>>>>
Please configure the ssid and password that need to be connected to WiFi in advance,in `main.c`.

.. code-block:: c
  #define ROUTER_SSID "your ssid"
  #define ROUTER_PWD "your password" 


Configure mqttt server
>>>>>>>>>
Please configure the server url  in `demo.c`.

.. code-block:: c

   axk_mqtt_client_config_t mqtt_cfg = {
    .uri="mqtts://mqtt.eclipseprojects.io:8883",
    .cert_pem=CA_CRT,
    .client_cert_pem=CLIENT_CRT,
    .client_key_pem=CLIENT_KEY,
    .event_handle = event_cb,
};

Troubleshooting
:::::::::

For any technical queries, please open an [issue](https://github.com/Ai-Thinker-Open/Ai-Thinker-WB2/issues) on GitHub. We will get back to you soon.
