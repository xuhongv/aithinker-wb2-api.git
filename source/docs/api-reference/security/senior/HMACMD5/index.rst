安信可 HMACMD5示例
======

.. raw:: html

   <style>
   body {counter-reset: h2}
     h2 {counter-reset: h3}
     h2:before {counter-increment: h2; content: counter(h2) ". "}
     h3:before {counter-increment: h3; content: counter(h2) "." counter(h3) ". "}
     h2.nocount:before, h3.nocount:before, { content: ""; counter-increment: none }
   </style>

--------------


=========
HMACMD5 Example
=========

This routine shows the `HMACMD5` verification process and the overall verification results,used to verify the correctness of process verification and overall verification.

Example Output
:::::::::

::

  ...(other log)
  [OS] Starting aos_loop_proc task...
  [OS] Starting OS Scheduler...
  INFO (5)[main.c:  41] Update process:hello
  INFO (7)[main.c:  43] Update process:-
  INFO (12)[main.c:  45] Update process:Ai-WB2-Kit
  INFO:(17)[main.c:  48] Segmental encryption:0000-000F: 7D 28 37 ED 18 C5 C0 6E  74 C9 18 20 E5 5A 37 7D    }(7....nt.. .Z7}
  INFO:(29)[main.c:  53] Overall encryption:0000-000F: 7D 28 37 ED 18 C5 C0 6E  74 C9 18 20 E5 5A 37 7D    }(7....nt.. .Z7}



Troubleshooting
:::::::::

For any technical queries, please open an [issue](https://github.com/Ai-Thinker-Open/Ai-Thinker-WB2/issues) on GitHub. We will get back to you soon.
