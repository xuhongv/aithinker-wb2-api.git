安信可  HMACSHA256示例
======

.. raw:: html

   <style>
   body {counter-reset: h2}
     h2 {counter-reset: h3}
     h2:before {counter-increment: h2; content: counter(h2) ". "}
     h3:before {counter-increment: h3; content: counter(h2) "." counter(h3) ". "}
     h2.nocount:before, h3.nocount:before, { content: ""; counter-increment: none }
   </style>

--------------


=========
 HMACSHA256  Example
=========

This routine demonstrates the routine of AEC's `ECB` and `CBC` modes.
Each mode shows how to perform `128 bit`, `192 bit` and `256 bit` encryption

Example Output
:::::::::

::

  ...(other log)
  [OS] Starting aos_loop_proc task...
  [OS] Starting OS Scheduler...
  INFO (5)[main.c:  41] Update process:hello
  INFO (8)[main.c:  43] Update process:-
  INFO (12)[main.c:  45] Update process:Ai-WB2-Kit
  INFO:(17)[main.c:  48] Segmental encryption:0000-001F: 6B 28 50 9A 09 55 E6 53  4E 00 7B BA BC 19 B4 4C  E5 0F 16 A4 3D EB DA 3A  3A B4 69 EC 1A EF 0E A7    k(P..U.SN.{....L....=..::.i.....
  INFO:(35)[main.c:  53] Overall encryption:0000-001F: 6B 28 50 9A 09 55 E6 53  4E 00 7B BA BC 19 B4 4C  E5 0F 16 A4 3D EB DA 3A  3A B4 69 EC 1A EF 0E A7    k(P..U.SN.{....L....=..::.i.....





Troubleshooting
:::::::::

For any technical queries, please open an [issue](https://github.com/Ai-Thinker-Open/Ai-Thinker-WB2/issues) on GitHub. We will get back to you soon.
