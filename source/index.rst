安信可 Ai-WB2 系列模组二次开发指南
=================================


| AiThinker WB2 Development Guide 是由安信可官方推出的针对 Ai-WB2 系列模组二次开发教程，以及API接口说明。

==================  ==================  ==================
|快速入门|_          |API参考|_         |API指南|_
------------------  ------------------  ------------------
`快速入门`_          `API参考`_         `API指南`_
==================  ==================  ==================

.. |快速入门| image:: _static/instruction.png
.. _快速入门: docs/instruction/index.html

.. |API参考| image:: _static/api-reference.png
.. _API参考: docs/api-reference/index.html

.. |API指南| image:: _static/api-guides.png
.. _API指南: docs/api-guides/index.html



联系我们
~~~~~~~~~~~~~~
1. 样品购买： https://anxinke.taobao.com
2. 样品资料： https://docs.ai-thinker.com
3. 商务合作： 0755-29162996
4. 公司地址： 深圳市宝安区西乡固戍华丰智慧创新港C栋403、408~410


.. toctree::
   :hidden:
   :maxdepth: 2
   :glob:

   docs/instruction/index
   docs/api-reference/index
   docs/api-guides/index
   